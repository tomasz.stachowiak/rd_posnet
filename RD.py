import time
import binascii
import socket
import sys
import re
import os

# Data bezaca i godzina
today = time.strftime("%Y-%m-%d");
tt = time.strftime("%H:%M:%S")
today_time = today + ' ' + tt

# zapis do pliku
def fileWrite(ftpData):
    file = open('C:\POSNET_RD\posnet_rd_log.txt', '+a')
    file.write(today_time +'\x09'+ ftpData+'\n')
    file.close()

#sprawdzenie rozmiaru logu i zalozenie nowego
def fileSize():
    try:
        file_size = os.path.getsize('C:\POSNET_RD\posnet_rd_log.txt')
    except:
        fileWrite('Brak pliku')
    else:
        if file_size > 10000:
            fileWrite('Zakładam nowy plik')
            os.remove('C:\POSNET_RD\posnet_rd_log_old.txt')
            os.rename('C:\POSNET_RD\posnet_rd_log.txt', 'C:\POSNET_RD\posnet_rd_log_old.txt')
        

# Adres i port drukarki
if len(sys.argv)> 2:
    HOST = sys.argv[1]
    HOST = HOST.replace(',','.')
    PORT = int(re.findall("\d+",sys.argv[2])[0])
else:
    fileWrite('START ---------------')
    fileWrite('Brak adresu IP lub nr portu (składnia RD.exe IPadress PORTnumber)')
    fileWrite('---------------STOP STOP')
    sys.exit()

# Rozkaz bez STX i ETX oraz # przed CRC
    # raport dobowy
dailyRep = bytearray('dailyrep\x09\x64\x61'+today+'\x09',"UTF-8")

    #status drukarki
statusReq = bytearray('scomm\x09',"UTF-8")

    #status drukarki 2 
endOfPrint = bytearray('hardprncancel\x09',"utf-8")    
statusReq2 = bytearray('sdev\x09',"UTF-8")

#oblicanie crc
def seq_crc(myObj):
    test_data = [myObj]
    def crc(fn):
        for x in test_data:
            res = fn(x)
        tekst= [(''+ myObj[0:22].decode() + '#' + f"{res:04X}" + '')]
        return tekst
    # https://docs.python.org/3/library/binascii.html
    def crc16_xmodem(data: bytes):
        return binascii.crc_hqx(data, 0)
    return(crc(crc16_xmodem))

#generowanie crc, zamian na string           
def seq_gen(Obj):
    a = seq_crc(Obj)
    sequence = listToStr = ' '.join(map(str, a))
    return sequence

#wysłanie danych do DF
def send_seq(sequence, HOST, PORT):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        result = s.connect_ex((HOST, PORT))
        if result:
            fileWrite('Problem z połączeniem - zweryfikuj adres IP - czy drukarka działa?')
            s.close()
            return(0)    
        else:
            sequence = seq_gen(sequence)
            fileWrite("wysyłam: " + sequence)
            fileWrite( '---------------STOP STOP')
            sequence = sequence.encode('utf-8')
            s.send(sequence)
            data_out = s.recv(1024)
            fileWrite("odebrane:" + data_out.decode('utf-8'))
            fileWrite('---------------STOP STOP')
            s.close()
            return data_out
    
#główna cześć programu
fileSize()
fileWrite("\n")
fileWrite('START ---------------') 
fileWrite('Sprawdzam połączenie z drukarką na adresie ' + str(HOST) + ' i porcie ' + str(PORT))

a = send_seq(statusReq,HOST,PORT)

if a == 0:
    fileWrite('Brak połączenia')
    fileWrite( '---------------STOP STOP')
    sys.exit()
else:
    try:
        b = (a.decode().index("tzT"))
    except:
        fileWrite('Jest sprzedaż - wykonuje raport')
        send_seq(dailyRep , HOST , PORT)
    else:
        fileWrite('Brak sprzedaży - nie wykonuje raportu')
fileWrite('---------------STOP STOP')
sys.exit()
